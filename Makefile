#
# Makefile 
# steering
#

CC=dmd
CFLAGS=-H -gc -unittest
CFLAGS_LIB=-lib

FILES=steering/point.d steering/game_object.d main.d
FILES_SEEK=steering/point.d steering/game_object.d main_seek.d
FILES_FLEE=steering/point.d steering/game_object.d main_flee.d
FILES_WANDER=steering/point.d steering/game_object.d main_wander.d
FILES_FLOCK=steering/point.d steering/game_object.d main_flock.d
FILES_FLOCK_VISUAL=steering/point.d steering/game_object.d main_flock_visual.d
FILES_LIB=steering/point.d steering/game_object.d
# -- glfw -- include LIBS=-L-lDerelictUtil -L-lDerelictGL3 -L-lDerelictGLFW3 -L-ldl
LIBS=-L-lDgame -L-lDerelictUtil -L-lDerelictGL3 -L-lDerelictSDL2 -L-ldl
# -- glfw -- include INCLUDE=-I${HOME}/d_apps/derelict_util/source -I${HOME}/d_apps/derelict_glfw3/source -I${HOME}/d_apps/derelict_gl3/source
INCLUDE=-I${HOME}/d_apps/dgame/source -I${HOME}/d_apps/derelict_util/source -I${HOME}/d_apps/derelict_gl3/source -I${HOME}/d_apps/derelict_sdl2/source
CLEAN=main main_seek main_flee main_wander main_flock main_flock_visual libSteering.a

all: main seek flee wander flock flock_visual lib

main: ${FILES}
	${CC} $^ -ofmain ${CFLAGS}

seek: ${FILES_SEEK}
	${CC} $^ -ofmain_seek ${CFLAGS}

flee: ${FILES_FLEE}
	${CC} $^ -ofmain_flee ${CFLAGS}

wander: ${FILES_WANDER}
	${CC} $^ -ofmain_wander ${CFLAGS}

flock: ${FILES_FLOCK}
	${CC} $^ -ofmain_flock ${CFLAGS}

flock_visual: ${FILES_FLOCK_VISUAL}
	${CC} $^ -ofmain_flock_visual ${CFLAGS} ${LIBS} ${INCLUDE}

lib: ${FILES_LIB}
	${CC} $^ -oflibSteering ${CFLAGS} ${CFLAGS_LIB}

clean: ${CLEAN}
	rm $^
