import std.stdio;
import core.thread;
import steering.point;
import steering.game_object;

void main() {
  int max = 25, sleep = 800;
  double angle = 0, angle_inc = 0.1, angle_max = 360;
  Point *circle_center = point_create(10, 10);
  Point *destination = point_create(20, 10);
  double diameter = point_distance(destination, circle_center);
  GameObject *craft = game_object_create(1.1, 1.1);
  craft.position[0].x = 15;
  craft.position[0].y = 10;
  foreach(i; 0..max) {
    writef("dest %f %f %f\n", destination.x, destination.y, point_distance(destination, circle_center));
    writef("craft %f %f\n", craft.position[0].x, craft.position[0].y);
    Thread.sleep(dur!("msecs")(sleep));
    angle += angle_inc;
    angle %= angle_max;
    point_rotate(destination, circle_center, angle, diameter);
    game_object_seek(craft, destination);
    game_object_move(craft);
    game_object_acceleration_remove(craft);
  }
  game_object_destroy(&craft);
  point_destroy(&circle_center);
  point_destroy(&destination);
}
