module steering.point;

import core.stdc.stdlib;
import std.math;

struct Point {
  double x;
  double y;
}

Point *point_create(double x = 0, double y = 0) {
  Point *that = cast(Point*)malloc(Point.sizeof);
  that.x = x;
  that.y = y;
  return that;
}
void point_destroy(Point **that) {
  if(*that == null) {
    return;
  }
  free(*that);
  *that = null;
}
void point_add_set(Point *that, Point *one, Point *two) {
  that.x = one.x + two.x;
  that.y = one.y + two.y;
}
Point *point_diff(Point *one, Point *two) {
  return point_create(one.x - two.x, one.y - two.y);
}
void point_diff_set(Point *that, Point *one, Point *two) {
  that.x = one.x - two.x;
  that.y = one.y - two.y;
}
void point_mult_scalar_set(T)(Point *that, Point *one, T scalar) {
  that.x = one.x * scalar;
  that.y = one.y * scalar;
}
void point_div_scalar_set(T)(Point *that, Point *one, T scalar) {
  that.x = one.x / scalar;
  that.y = one.y / scalar;
}
void point_normalize_set(Point *that, Point *one) {
  if(!one.x && !one.y) {
    return;
  }
  double length = point_length(one);
  that.x /= length;
  that.y /= length;
}
void point_truncate_set(Point *that, Point *one, double diameter) {
  double length = point_length(one);
  if(diameter < length) {
    length = diameter / length;
    point_mult_scalar_set(that, one, length);
  }
}
double point_length(Point *that) {
  return sqrt(that.x * that.x + that.y * that.y);
}
double point_distance(Point *one, Point *two) {
  double diff_x = pow(one.x - two.x, 2);
  double diff_y = pow(one.y - two.y, 2);
  double _sqrt = sqrt(diff_x + diff_y);
  return _sqrt;
}
double point_distance_f(double x_one, double y_one, double x_two, double y_two) {
  Point *one = point_create(x_one, y_one);
  Point *two = point_create(x_two, y_two);
  double dist = point_distance(one, two);
  point_destroy(&one);
  point_destroy(&two);
  return dist;
}
double point_dot_product(Point *one, Point *two) {
  double x = one.x * two.x;
  double y = one.y * two.y;
  return x + y;
}
double point_angle(Point *one, Point *two) {
  double dot = point_dot_product(one, two);
  double len = point_length(one) * point_length(two);
  double angle = dot / len;
  return acos(angle);
  // 1 rad 57 degrees about
}
void point_rotate(Point *that, Point *center, double angle, double diameter) {
  // cos - x adj over diag
  // sin - y opposite over diag
  that.x = center.x + cos(angle) * diameter;
  that.y = center.y + sin(angle) * diameter;
}
