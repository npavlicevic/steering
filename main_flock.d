import std.stdio;
import core.thread;
import steering.point;
import steering.game_object;

void main() {
  int max = 25, sleep = 800;
  double neighbor_radius = 10;
  GameObject*[] crafts = [
    game_object_create(1.1, 1.1), 
    game_object_create(1.1, 1.1), 
    game_object_create(1.1, 1.1), 
    game_object_create(1.1, 1.1), 
    game_object_create(1.1, 1.1)
  ];
  crafts[0].position[0].x = 7;
  crafts[0].position[0].y = 7;
  crafts[1].position[0].x = 1;
  crafts[1].position[0].y = 1;
  crafts[2].position[0].x = 2;
  crafts[2].position[0].y = 2;
  crafts[3].position[0].x = 3;
  crafts[3].position[0].y = 3;
  crafts[4].position[0].x = 4;
  crafts[4].position[0].y = 4;
  foreach(i; 0..max) {
    foreach(craft; crafts) {
      game_object_cohere(craft, crafts, neighbor_radius);
      game_object_align(craft, crafts, neighbor_radius);
      game_object_separate(craft, crafts, neighbor_radius);
    }
    foreach(j, craft; crafts) {
      game_object_move(craft);
      game_object_acceleration_remove(craft);
      writef("craft %d position %f %f\n", j, craft.position[0].x, craft.position[0].y);
    }
    Thread.sleep(dur!("msecs")(sleep));
  }
  foreach(craft; crafts) {
    game_object_destroy(&craft);
  }
}
